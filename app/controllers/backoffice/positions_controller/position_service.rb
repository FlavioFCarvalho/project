class Backoffice::PositionsController::PositionService
  attr_accessor :position

  def self.create(params_position)
    @position = Position.new(params_position)

    if @position.valid?
      @position.save!
    end  
    @position        
  end
end