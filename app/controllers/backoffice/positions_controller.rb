class Backoffice::PositionsController < BackofficeController
  before_action :set_position, only: [:edit,:update]
  
  def index
    @positions = Position.all
  end

  def new
  	@position = Position.new
  end

  def create	
    @position = PositionService.create(params_position)
    
    #Se não existir erros
    unless @position.errors.any?
       redirect_to backoffice_positions_path, notice: "O cargo (#{@position.description}) foi cadastrado com sucesso!"
    else
      render :new
    end  
  end	

  def edit
    
  end
  
  def update
  	if @position.update	(params_position)
      redirect_to backoffice_positions_path, notice: "O cargo (#{@position.description}) foi atualizado com sucesso!"
    else
      render :edit
    end  
  end

  private

  def set_position
    @position = Position.find(params[:id])
  end
  

  def params_position
    params.require(:position).permit(:description)
  end	
end
