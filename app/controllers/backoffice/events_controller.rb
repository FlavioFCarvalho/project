class Backoffice::EventsController < BackofficeController 
  before_action :set_event, only: [:edit,:update]
  
  def index
    @events = Event.all
  end

  def new
    @event = Event.new
  end

  def create  
    @event = Event.new(params_event)
    if @event.save
        redirect_to backoffice_events_path, notice: "O evento (#{@event.name}) foi cadastrado com sucesso!"
    else
      render :new
    end  
  end 

  def edit
    
  end

  def update
    if @event.update (params_event)
      redirect_to backoffice_event_path, notice: "O evento (#{@event.name}) foi atualizado com sucesso!"
    else
      render :edit
    end  
  end

  private

  def set_event
    @event = Event.find(params[:id])
  end

  def params_event
    params.require(:event).permit(:name, :description, :date_event, :avatar)
  end 
end
