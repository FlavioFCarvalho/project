require 'test_helper'

class Backoffice::PositionsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get backoffice_positions_index_url
    assert_response :success
  end

end
