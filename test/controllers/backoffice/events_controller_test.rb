require 'test_helper'

class Backoffice::EventsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get backoffice_events_index_url
    assert_response :success
  end

end
