require 'test_helper'

class Backoffice::AccountabilitiesControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get backoffice_accountabilities_index_url
    assert_response :success
  end

end
