Rails.application.routes.draw do
  
  get  'backoffice', to: 'backoffice/dashboard#index'

  namespace :backoffice do
    resources :accountabilities, except: [:show]
    resources :admins, except: [:show]
    resources :events , except: [:show]
    resources  :positions
    get 'dashboard', to:'dashboard#index'
 end
  
  namespace :site do
    get 'home/index'
  end

  devise_for :admins, :skip => [:registrations]
  devise_for :members

  root 'site/home#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
